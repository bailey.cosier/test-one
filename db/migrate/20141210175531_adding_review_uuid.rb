class AddingReviewUuid < ActiveRecord::Migration
  def change
    add_column :reviews, :uuid, :string, null: false
  end
end
